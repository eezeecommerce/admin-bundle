<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\AdminBundle\Controller;

use eezeecommerce\OrderBundle\eezeecommerceOrderBundle;
use eezeecommerce\OrderBundle\Entity\OrderNotes;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class OrdersController extends Controller
{

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     * @todo orderDetailsAction needs the order number as slug - replace /test.
     */
    public function indexAction()
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->findAllOrders();

        $settings = $this->get("eezeecommerce_settings.setting");

        return $this->render(
            "eezeecommerceAdminBundle:Orders:index.html.twig",
            ["orders" => $orders, "settings" => $settings->loadSettingsBySite()]
        );
    }

    public function indexCompletedAction()
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->findAllCompletedOrders();

        $settings = $this->get("eezeecommerce_settings.setting");

        return $this->render(
            "eezeecommerceAdminBundle:Orders:indexcompleted.html.twig",
            ["orders" => $orders, "settings" => $settings->loadSettingsBySite()]
        );
    }

    public function shippedAjaxDataAction()
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->findAllShippedOrders();

        $settings = $this->get("eezeecommerce_settings.setting");
        $setting = $settings->loadSettingsBySite();

        $prefix = $setting->getOrderPrefix()."-";
        $suffix = $setting->getOrderSuffix();

        $array = [];
        foreach ($orders as $order) {
            $tmp = [];

            $tmp[] = $order->getId();

            $tmp[] = "<a href='".$this->generateUrl(
                    "_eezeecommerce_admin_orders_details",
                    array('id' => $order->getId())
                )."'>".
                $prefix.$order->getOrderNumber().$suffix
                ."</a>";
            if (null !== $order->getUser()) {
                $url = $this->generateUrl('_eezeecommerce_admin_customers_modify', ["id" => $order->getUser()->getId()]);
                $tmp[] = "<a href='".$url."'>".ucfirst($order->getBillingAddress()->getNameFirst())." ".ucfirst($order->getBillingAddress()->getNameLast())."</a>";
            } else {
                $tmp[] = ucfirst($order->getBillingAddress()->getNameFirst())." ".ucfirst($order->getBillingAddress()->getNameLast());
            }
            $orderDate = $order->getOrderDate();
            $tmp[] = $orderDate->format("d/m/Y \\: H:s");
            $shippedDate = $order->getShippedTs();
            if (null !== $shippedDate) {
                $tmp[] = $shippedDate->format("d/m/Y \\: H:s");
            } else {
                $tmp[] = null;
            }
            $array["data"][] = $tmp;
        }

        return new JsonResponse($array);
    }

    public function indexShippedAction()
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->findAllShippedOrders();

        $settings = $this->get("eezeecommerce_settings.setting");

        return $this->render(
            "eezeecommerceAdminBundle:Orders:indexshipped.html.twig",
            ["orders" => $orders, "settings" => $settings->loadSettingsBySite()]
        );
    }

    public function indexPendingAction()
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->findAllPendingOrders();

        $settings = $this->get("eezeecommerce_settings.setting");

        return $this->render(
            "eezeecommerceAdminBundle:Orders:indexpending.html.twig",
            ["orders" => $orders, "settings" => $settings->loadSettingsBySite()]
        );
    }

    public function indexAbandonedAction()
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->findAllAbandonedOrders();

        $settings = $this->get("eezeecommerce_settings.setting");

        return $this->render(
            "eezeecommerceAdminBundle:Orders:indexabandoned.html.twig",
            ["orders" => $orders, "settings" => $settings->loadSettingsBySite()]
        );
    }

    public function orderDetailsAction($id)
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $settings = $this->get("eezeecommerce_settings.setting");

        $settings = $settings->loadSettingsBySite();
        $em = $this->getDoctrine()->getManager();

        $settings = $em->merge($settings);

        return $this->render(
            "eezeecommerceAdminBundle:Orders:orderdetails.html.twig",
            ["orders" => $orders, "settings" => $settings]
        );
    }

    public function orderAddAction()
    {
        /*  $order = new Order();
          $form = $this->createForm(new OrderType(), $order);

          $form->handleRequest($request);

          if($form->isValid()) {
          $oredr->save();
          return $this->redirect($this->generateUrl("_eezeecommerce_admin_orders_index", array(
          "custnum" => $user->getId(),
          )));
          }

         */
        return $this->render("eezeecommerceAdminBundle:Orders:orderadd.html.twig");
    }

    public function orderInvoiceAction($id)
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $settings = $this->get("eezeecommerce_settings.setting");

        return $this->render(
            "AppBundle:Admin:invoice.html.twig",
            ["orders" => $orders, "settings" => $settings->loadSettingsBySite()]
        );
    }

    public function orderPackingslipAction($id)
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $settings = $this->get("eezeecommerce_settings.setting");

        return $this->render(
            "AppBundle:Admin:packingslip.html.twig",
            ["orders" => $orders, "settings" => $settings->loadSettingsBySite()]
        );
    }

    public function orderShipAction($id)
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $orders->setShipped(1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($orders);
        $em->flush();

        $to = $orders->getBillingAddress()->getContactEmail();
        $params = [
            "orders" => $orders,
        ];
        $locale = "en";

        $message = $this->get('lexik_mailer.message_factory')->get('order-shipped', $to, $params, $locale);

        $this->get('mailer')->send($message);

        $this->addFlash(
            'success',
            "Item shipped successfully."
        );

        return $this->redirectToRoute("_eezeecommerce_admin_orders_details", ["id" => $id]);
    }

    public function orderUnshipAction($id)
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $orders->setShipped(0);

        $em = $this->getDoctrine()->getManager();
        $em->persist($orders);
        $em->flush();

        $this->addFlash(
            'success',
            "Item unshipped successfully."
        );

        return $this->redirectToRoute("_eezeecommerce_admin_orders_details", ["id" => $id]);
    }

    public function orderDeleteAction($id)
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $orders->setDeleted(1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($orders);
        $em->flush();

        $to = $orders->getBillingAddress()->getContactEmail();
        $params = [
            "orders" => $orders,
        ];
        $locale = "en";

        $message = $this->get('lexik_mailer.message_factory')->get('order-deleted', $to, $params, $locale);

        $this->get('mailer')->send($message);

        $this->addFlash(
            'success',
            "Item deleted successfully."
        );

        return $this->redirectToRoute("_eezeecommerce_admin_orders_details", ["id" => $id]);
    }

    public function orderSendInvoiceAction($id)
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $settings = $this->get("eezeecommerce_settings.setting");

        $to = $orders->getBillingAddress()->getContactEmail();
        $params = [
            "orders" => $orders,
            "settings" => $settings->loadSettingsBySite(),
        ];
        $locale = "en";

        $message = $this->get('lexik_mailer.message_factory')->get('order-success', $to, $params, $locale);

        $this->get('mailer')->send($message);

        $this->addFlash(
            'success',
            "Invoice emailed successfully."
        );

        return $this->redirectToRoute("_eezeecommerce_admin_orders_details", ["id" => $id]);
    }

    public function orderSetFlagAction($id, $flag)
    {
        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        if ($flag != "none" && $flag != "green" && $flag != "blue" && $flag != "orange" && $flag != "purple" && $flag != "red" && $flag != "yellow" && $flag != "gray") {
            $this->addFlash(
                'warning',
                "Invalid Flag set. Please try again."
            );

            return $this->redirectToRoute("_eezeecommerce_admin_orders_details", ["id" => $id]);
        }
        $orders->setFlag($flag);
        $em = $this->getDoctrine()->getManager();
        $em->persist($orders);
        $em->flush();

        return $this->redirectToRoute("_eezeecommerce_admin_orders_details", ["id" => $id]);
    }

    public function orderBatchShipAction(Request $request)
    {
        if (!$request->request->has("json")) {
            return new JsonResponse(false);
        }

        $json = json_decode($request->request->get("json"), true);

        $em = $this->getDoctrine()->getManager();
        foreach ($json as $item) {
            $orders = $this->getDoctrine()
                ->getRepository('eezeecommerceOrderBundle:Orders')
                ->find($item);
            $orders->setShipped(1);

            $to = $orders->getBillingAddress()->getContactEmail();
            $params = [
                "orders" => $orders,
            ];
            $locale = "en";

            $message = $this->get('lexik_mailer.message_factory')->get('order-shipped', $to, $params, $locale);

            $this->get('mailer')->send($message);
        }

        $em->persist($orders);
        $em->flush();

        return new JsonResponse(true);
    }

    public function orderBatchUnShipAction(Request $request)
    {
        if (!$request->request->has("json")) {
            return new JsonResponse(false);
        }

        $json = json_decode($request->request->get("json"), true);

        $em = $this->getDoctrine()->getManager();
        foreach ($json as $item) {
            $orders = $this->getDoctrine()
                ->getRepository('eezeecommerceOrderBundle:Orders')
                ->find($item);
            $orders->setShipped(0);
        }

        $em->persist($orders);
        $em->flush();

        return new JsonResponse(true);
    }

    public function orderBatchDeleteAction(Request $request)
    {
        if (!$request->request->has("json")) {
            return new JsonResponse(false);
        }

        $json = json_decode($request->request->get("json"), true);

        $em = $this->getDoctrine()->getManager();
        foreach ($json as $item) {
            $orders = $this->getDoctrine()
                ->getRepository('eezeecommerceOrderBundle:Orders')
                ->find($item);
            $orders->setDeleted(1);
        }

        $em->persist($orders);
        $em->flush();

        return new JsonResponse(true);
    }

    public function orderBatchPrintInvoiceAction(Request $request)
    {
        if (!$request->request->has("id") ||
            (!$request->request->has("invoice") &&
                !$request->request->has("packing"))
        ) {
            return $this->redirect($this->generateUrl("_eezeecommerce_admin_orders_index_completed"));
        }

        $idArray = $request->request->get("id");

        $orderArray = $this->getDoctrine()->getRepository('eezeecommerceOrderBundle:Orders')
            ->findBy(array("id" => $idArray));


        $settings = $this->get("eezeecommerce_settings.setting");

        if ($request->request->has("invoice")) {
            return $this->render(
                "AppBundle:Admin:invoice_batch.html.twig",
                ["orderArray" => $orderArray, "settings" => $settings->loadSettingsBySite()]
            );
        }

        return $this->render(
            "AppBundle:Admin:packingslip_batch.html.twig",
            ["orderArray" => $orderArray, "settings" => $settings->loadSettingsBySite()]
        );
    }

    public function orderBatchFlagAction(Request $request, $flag)
    {

        if ($flag != "none" && $flag != "green" && $flag != "blue" && $flag != "orange" && $flag != "purple" && $flag != "red" && $flag != "yellow" && $flag != "gray") {
            $this->addFlash(
                'warning',
                "Invalid Flag set. Please try again."
            );

            return $this->redirectToRoute("_eezeecommerce_admin_orders_index_completed");
        }

        $idArray = $request->request->get("id");

        foreach ($idArray as $item) {
            $orders = $this->getDoctrine()->getRepository('eezeecommerceOrderBundle:Orders')
                ->find($item);

            $orders->setFlag($flag);
            $em = $this->getDoctrine()->getManager();
            $em->persist($orders);
        }
        $em->flush();

        return $this->redirectToRoute("_eezeecommerce_admin_orders_index_completed");
    }

    public function orderNoteAddAction(Request $request, $id)
    {

        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $settings = $this->get("eezeecommerce_settings.setting");

        $ordernote = new OrderNotes();
        $subject = $request->request->get('notesubject');
        $message = $request->request->get('notemessage');

        $ordernote->setMessage($message);
        $ordernote->setSubject($subject);
        $ordernote->setOrder($orders);
        $ordernote->setUser($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->persist($ordernote);
        $em->flush();

        return $this->redirectToRoute("_eezeecommerce_admin_orders_details", ["id" => $id]);
    }

}
