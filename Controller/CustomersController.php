<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use eezeecommerce\UserBundle\Entity\User;
use eezeecommerce\UserBundle\Form\UserAdminType;
use eezeecommerce\UserBundle\Form\UserAdminModifyType;
use eezeecommerce\UserBundle\Form\GroupsType;
use eezeecommerce\UserBundle\Entity\Groups;

class CustomersController extends Controller
{

    public function indexAction()
    {
        $customers = $this->getDoctrine()
            ->getRepository('eezeecommerceUserBundle:User')
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Customers:index.html.twig", ["customers" => $customers]);
    }

    public function customerAddAction(Request $request)
    {
        $user = new User();
        $usertype = new UserAdminType();

        $form = $this->createForm($usertype, $user)->add("Save", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_customers_modify", ["id" => $user->getId()]);
        }

        return $this->render("eezeecommerceAdminBundle:Customers:customeradd.html.twig", ["form" => $form->createView()]);
    }

    public function customerModifyAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('eezeecommerceUserBundle:User')
            ->find($id);
        $usertype = new UserAdminModifyType();

        if (!$user) {
            throw $this->createNotFoundException("User Not Found");
        }

        $form = $this->createForm($usertype, $user)->add("Update", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_customers_index");
        }

        return $this->render("eezeecommerceAdminBundle:Customers:customermodify.html.twig", ["form" => $form->createView(), "user" => $user]);
    }

    public function customerGroupsIndexAction()
    {
        $customergroups = $this->getDoctrine()
            ->getRepository('eezeecommerceUserBundle:Groups')
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Customers:Groups/index.html.twig", ["customergroups" => $customergroups]);
    }

    public function customerGroupsModifyAction(Request $request, $id)
    {
        $group = $this->getDoctrine()->getRepository('eezeecommerceUserBundle:Groups')
            ->find($id);
        $grouptype = new GroupsType();

        if (!$group) {
            throw $this->createNotFoundException("Group Not Found");
        }

        $form = $this->createForm($grouptype, $group)->add("Update", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_customers_groups_index");
        }

        return $this->render("eezeecommerceAdminBundle:Customers:Groups/groupmodify.html.twig", ["group" => $group, "form" => $form->createView(), "id" => $id]);
    }

    public function customerGroupsAddAction(Request $request)
    {
        $groups = new Groups();
        $groupstype = new GroupsType();

        $form = $this->createForm($groupstype, $groups)->add("Save", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($groups);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_customers_groups_modify", ["id" => $groups->getId()]);
        }

        return $this->render("eezeecommerceAdminBundle:Customers:Groups/groupadd.html.twig", ["form" => $form->createView()]);
    }

}
