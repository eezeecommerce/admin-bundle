<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use eezeecommerce\SettingsBundle\Entity\Settings;
use eezeecommerce\SettingsBundle\Form\SettingsType;
use eezeecommerce\ShippingBundle\Entity\Courier;
use eezeecommerce\ShippingBundle\Form\CourierType;
use eezeecommerce\ShippingBundle\Entity\CourierServices;
use eezeecommerce\ShippingBundle\Form\CourierServicesType;
use Doctrine\Common\Collections\ArrayCollection;
use eezeecommerce\SettingsBundle\Form\TaxesSettingsType;
use eezeecommerce\TaxBundle\Form\TaxRatesType;
use eezeecommerce\CurrencyBundle\Entity\Currency;
use eezeecommerce\CurrencyBundle\Form\CurrencyType;

class SettingsController extends Controller
{

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function generalAction(Request $request)
    {
        $settings = $this->getDoctrine()->getRepository("eezeecommerceSettingsBundle:Settings")
            ->getSettings();
        if (null === $settings) {
            $settings = new Settings();
        }
        $form = $this->createForm(new SettingsType(), $settings);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settings);
            $em->flush();

            return $this->redirect($this->generateUrl("_eezeecommerce_admin_settings_general"));
        }

        return $this->render("eezeecommerceAdminBundle:Settings:general.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function paymentsAction()
    {
        $form = $this->createForm(new PaymentsSettingsType());

        return $this->render("eezeecommerceAdminBundle:Settings:payments.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function checkoutAction()
    {
        $form = $this->createForm(new CheckoutSettingsType());

        return $this->render("eezeecommerceAdminBundle:Settings:checkout.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function taxAction(Request $request)
    {

        $taxSettings = $this->getDoctrine()->getRepository("eezeecommerceSettingsBundle:TaxesSettings")
            ->getTaxSettings();
        if (null === $taxSettings) {

            $settings = $this->get("eezeecommerce_settings.setting");
            $taxSettings = new \eezeecommerce\SettingsBundle\Entity\TaxesSettings();
            $taxSettings->setSettings($settings->loadSettingsBySite());
        }

        $form = $this->createForm(new TaxesSettingsType(), $taxSettings);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($taxSettings);
            $em->flush();

            return $this->redirect($this->generateUrl("_eezeecommerce_admin_settings_tax"));
        }

        $taxrates = $this->getDoctrine()->getRepository('eezeecommerce\TaxBundle\Entity\TaxRates')
            ->findAll();


        return $this->render("eezeecommerceAdminBundle:Settings/Tax:tax.html.twig", ["form" => $form->createView(), "taxrates" => $taxrates]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function taxrateAddAction(Request $request)
    {

        $settings = $this->get("eezeecommerce_settings.setting")->loadSettingsBySite();

        $taxSettings = $settings->getTaxSettings();

        $tax = new \eezeecommerce\TaxBundle\Entity\TaxRates();

        $tax->setTaxSettings($taxSettings);
        $form = $this->createForm(new TaxRatesType(), $tax);

        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($tax);


        if (count($errors) > 0) {
            return $this->render("eezeecommerceAdminBundle:Settings/Tax:taxrateadd.html.twig", [
                "form" => $form->createView(),
                "errors" => $errors]);
        }

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($tax->getCountry() as $country) {
                $country->setTaxRate($tax);
                $em->persist($country);
            }

            $em->persist($tax);
            $em->flush();

            return $this->redirect($this->generateUrl("_eezeecommerce_admin_settings_tax"));
        }

        return $this->render("eezeecommerceAdminBundle:Settings/Tax:taxrateadd.html.twig", ["form" => $form->createView()]);
    }

    public function taxrateModifyAction(Request $request, $id)
    {
        $taxType = new TaxRatesType();

        $tax = $this->getDoctrine()->getRepository('eezeecommerce\TaxBundle\Entity\TaxRates')
            ->find($id);

        $form = $this->createForm($taxType, $tax);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($tax);
            $em->flush();


            return $this->redirectToRoute("_eezeecommerce_admin_settings_tax");
        }

        return $this->render("eezeecommerceAdminBundle:Settings/Tax:taxratemodify.html.twig", ["form" => $form->createView(), "tax" => $tax]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function currenciesIndexAction()
    {

        $currencies = $this->getDoctrine()->getRepository('eezeecommerce\CurrencyBundle\Entity\Currency')
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Settings/Currency:index.html.twig", ["currencies" => $currencies]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function currenciesAddAction(Request $request)
    {

        $currencies = new Currency();

        $currenciesType = new CurrencyType();

        $form = $this->createForm($currenciesType, $currencies)->add("Save", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($currencies);
            $em->flush();


            return $this->redirectToRoute("_eezeecommerce_admin_settings_currencies");
        }

        return $this->render("eezeecommerceAdminBundle:Settings/Currency:currencyadd.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function currenciesModifyAction(Request $request, $id)
    {

        $currenciesType = new CurrencyType();

        $currencies = $this->getDoctrine()->getRepository('eezeecommerce\CurrencyBundle\Entity\Currency')
            ->find($id);

        $form = $this->createForm($currenciesType, $currencies)->add("Save", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($currencies);
            $em->flush();


            return $this->redirectToRoute("_eezeecommerce_admin_settings_currencies");
        }

        return $this->render("eezeecommerceAdminBundle:Settings/Currency:currencymodify.html.twig", ["form" => $form->createView(), "currencies" => $currencies]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function shippingAction()
    {
        $couriers = $this->getDoctrine()
            ->getRepository('eezeecommerceShippingBundle:Courier')
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Settings/Shipping:index.html.twig", ["couriers" => $couriers]);
    }

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function shippingCourierAddAction(Request $request)
    {
        $courier = new Courier();
        $couriertype = new CourierType();

        $form = $this->createForm($couriertype, $courier)->add("Save", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($courier);
            $em->flush();


            return $this->redirectToRoute("_eezeecommerce_admin_settings_shipping");
        }

        return $this->render("eezeecommerceAdminBundle:Settings/Shipping:courieradd.html.twig", ["form" => $form->createView()]);
    }

    public function shippingCourierModifyAction(Request $request, $id)
    {
        $couriertype = new CourierType();
        $couriers = $this->getDoctrine()
            ->getRepository('eezeecommerceShippingBundle:Courier')
            ->find($id);

        if (!$couriers) {
            throw $this->createNotFoundException("Courier Not Found");
        }

        $form = $this->createForm($couriertype, $couriers)->add("Update", "submit");


        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($couriers);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_settings_shipping");
        }

        return $this->render("eezeecommerceAdminBundle:Settings/Shipping:courierdetails.html.twig", ["courier" => $couriers, "form" => $form->createView()]);
    }

    public function shippingServiceAddAction(Request $request)
    {
        $service = new CourierServices();
        $servicetype = new CourierServicesType();

        $form = $this->createForm($servicetype, $service)->add("Save", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($service);
            $em->flush();


            return $this->redirectToRoute("_eezeecommerce_admin_settings_shipping");
        }

        return $this->render("eezeecommerceAdminBundle:Settings/Shipping:serviceadd.html.twig", ["form" => $form->createView()]);
    }

    public function shippingServiceModifyAction(Request $request, $id)
    {
        $couriertype = new CourierServicesType();
        $couriers = $this->getDoctrine()
            ->getRepository('eezeecommerceShippingBundle:CourierServices')
            ->find($id);

        if (!$couriers) {
            throw $this->createNotFoundException("Courier Service Not Found");
        }

        $originalCriteria = new ArrayCollection();

        foreach ($couriers->getServicePricing() as $option) {
            $originalCriteria->add($option);
        }

        $form = $this->createForm($couriertype, $couriers)->add("Update", "submit");


        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($originalCriteria as $option) {
                if (false === $couriers->getServicePricing()->contains($option)) {
                    $em->remove($option);
                }
            }

            $em->persist($couriers);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_settings_shipping");
        }

        return $this->render("eezeecommerceAdminBundle:Settings/Shipping:servicedetails.html.twig", ["courier" => $couriers, "form" => $form->createView()]);
    }

    public function shippingServiceDeleteAction(Request $request, $id)
    {
        $couriers = $this->getDoctrine()
            ->getRepository('eezeecommerceShippingBundle:CourierServices')
            ->find($id);

        if (!$couriers) {
            throw $this->createNotFoundException("Courier Service Not Found");
        }

        $em = $this->getDoctrine()->getManager();
        foreach ($couriers->getServicePricing() as $item) {
            $em->remove($item);
        }
        $em->remove($couriers);
        $em->flush();

        $this->addFlash(
            'success', "Courier Service deleted successfully."
        );

        return $this->redirectToRoute("_eezeecommerce_admin_settings_shipping");
    }

    public function countriesAction()
    {
        return $this->render("eezeecommerceAdminBundle:Settings/Countries:index.html.twig");
    }

    public function countriesDataAction()
    {
        $array = [];

        $countries = $this->getDoctrine()->getRepository("eezeecommerceShippingBundle:Country")->findBy([], ["sort" => "ASC", "name" => "ASC"]);
        foreach ($countries as $country) {
            $array[] = [
                "id" => $country->getId(),
                "text" => $country->getName(),
                "children" => false,
                "state" => [
                    "opened" => false
                ],
            ];
        }

        return new JsonResponse($array);
    }

    public function countriesMoveAction(Request $request)
    {
        if (!$request->request->has("json")) {
            return new JsonResponse(false);
        }

        $json = json_decode($request->request->get("json"), true);

        $em = $this->getDoctrine()->getManager();

        $i = 1;
        foreach ($json as $countryData) {
            $country = $this->getDoctrine()->getRepository("eezeecommerceShippingBundle:Country")
                ->find($countryData["id"]);
            $country->setSort($i);
            $em->persist($country);

            $i++;
        }

        $em->flush();

        return new JsonResponse(true);
    }

}
