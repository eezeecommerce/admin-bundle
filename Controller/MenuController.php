<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */
namespace eezeecommerce\AdminBundle\Controller;

use eezeecommerce\AdminBundle\AdminEvents;
use eezeecommerce\AdminBundle\Event\AdminMenuEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of MenuController
 *
 * @author root
 */
class MenuController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCompanyNameAction()
    {

        $settings = $this->get("eezeecommerce_settings.setting");

        return $this->render(
            'eezeecommerceAdminBundle:Menus:companyname.html.twig',
            ["settings" => $settings->loadSettingsBySite()]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCurrencyCodeAction()
    {
        return $this->render('eezeecommerceAdminBundle:Menus:currencycode.html.twig');
    }

    /**
     * Returns Admin Sidebar
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAdminMenuAction()
    {
        $dispatcher = $this->get("event_dispatcher");

        $menu = array(
            "dashboard" => array(
                "name" => "Dashboard",
                "icon" => "icon-home",
                "link" => "_eezeecommerce_admin_homepage",
                "children" => null
            ),
            "orders" => array(
                "name" => "Orders",
                "icon" => "icon-basket",
                "link" => "#",
                "children" => array(
                    array(
                        "link" => "_eezeecommerce_admin_orders_index_completed",
                        "name" => "Paid Orders"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_orders_index_shipped",
                        "name" => "Shipped Orders"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_orders_index_pending",
                        "name" => "Incomplete Orders"
                    )

                )
            ),
            "products" => array(
                "name" => "Products",
                "icon" => "icon-tag",
                "link" => "#",
                "children" => array(
                    array(
                        "link" => "_eezeecommerce_admin_products_index",
                        "name" => "Products List"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_products_disabled",
                        "name" => "Disabled Products List"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_products_inventory",
                        "name" => "Stock Inventory"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_products_options_index",
                        "name" => "Product Options"
                    )

                )
            ),
            "customers" => array(
                "name" => "Customers",
                "icon" => "icon-users",
                "link" => "#",
                "children" => array(
                    array(
                        "link" => "_eezeecommerce_admin_customers_index",
                        "name" => "Customers"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_customers_groups_index",
                        "name" => "Groups"
                    )

                )
            ),
            "reports" => array(
                "name" => "Reports",
                "icon" => "icon-graph",
                "link" => "_eezeecommerce_admin_reports_index",
                "children" => null
            ),
            "webstore" => array(
                "name" => "Web Store",
                "icon" => "icon-screen-desktop",
                "link" => "#",
                "children" => array(
                    array(
                        "link" => "_eezeecommerce_admin_webstore_category",
                        "name" => "Categories"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_webstore_category_price",
                        "name" => "Category Price Filters"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_webstore_discounts",
                        "name" => "Discounts"
                    )

                )
            ),
            "emails" => array(
                "name" => "Emails and Templates",
                "icon" => "icon-envelope",
                "link" => "#",
                "children" => array(
                    array(
                        "link" => "lexik_mailer.email_list",
                        "name" => "Email List"
                    ),
                    array(
                        "link" => "lexik_mailer.layout_list",
                        "name" => "Layout List"
                    )

                )
            ),
            "settings" => array(
                "name" => "Settings",
                "icon" => "icon-settings",
                "link" => "#",
                "children" => array(
                    array(
                        "link" => "_eezeecommerce_admin_settings_general",
                        "name" => "General"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_settings_countries",
                        "name" => "Countries"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_settings_shipping",
                        "name" => "Shipping"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_settings_currencies",
                        "name" => "Currencies and Exchange Rates"
                    ),
                    array(
                        "link" => "_eezeecommerce_admin_settings_tax",
                        "name" => "Taxes and VAT"
                    )

                )
            )
        );

        $event = new AdminMenuEvent($menu);

        $dispatcher->dispatch(AdminEvents::ADMIN_MENU_INITIALISE, $event);

        $dispatcher->dispatch(AdminEvents::ADMIN_MENU_COMPLETED, $event);

        return $this->render("eezeecommerceAdminBundle:Menus:sidebar.html.twig", ["menu" => $event->getMenu()]);
    }
}
