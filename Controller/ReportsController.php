<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ReportsController extends Controller
{

    public function indexAction()
    {

        return $this->render("eezeecommerceAdminBundle:Reports:index.html.twig");
    }

}
