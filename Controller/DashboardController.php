<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function indexAction()
    {
        $t = date("Y-m-d");
        $w = date("Y-m-d", time() - (60 * 60 * 24 * 7));
        $tw = date("Y-m-d", time() - (60 * 60 * 24 * 14));
        $m = date("Y-m-d", time() - (60 * 60 * 24 * 30));

        $today = $this->getDoctrine()->getRepository("eezeecommerceOrderBundle:Orders")
            ->dashboardOrders($t, $t);
        $week = $this->getDoctrine()->getRepository("eezeecommerceOrderBundle:Orders")
            ->dashboardOrders($w, $t);

        $stats = $this->getDoctrine()->getRepository("eezeecommerceOrderBundle:Orders")
            ->dashboardSummaryOrders($tw, $t);
        $i = 0;
        for ($i; $i <= 14; $i++) {
            $result = false;
            $date = date("Y-m-d", time() - (60 * 60 * 24 * $i));
            foreach ($stats as $stat) {
                if (array_search($date, $stat)) {
                    $result = true;
                    continue;
                }
            }

            if (!$result) {
                $stats[] = [
                    "x" => $date,
                    "a" => 0.00,
                    "c" => 0,
                ];
            }
        }
        sort($stats);

        //demo web data - needs changing


        $i2 = 0;
        for ($i2; $i2 <= 14; $i2++) {
            $d = date("Y-m-d", time() - (60 * 60 * 24 * $i));
            $webdata[] = [
                "d" => $d,
                "c" => rand(2, 5),
                "v" => rand(300, 500),
            ];
        }

        return $this->render(
            "eezeecommerceAdminBundle:Dashboard:index.html.twig",
            ["week" => $week, "today" => $today, "stats" => $stats, "webdata" => $webdata]
        );
    }

}
