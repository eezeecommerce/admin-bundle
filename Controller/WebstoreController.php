<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use eezeecommerce\CategoryBundle\Entity\CategoryPriceFilters;
use eezeecommerce\CategoryBundle\Form\CategoryPriceFiltersType;
use eezeecommerce\DiscountBundle\Entity\DiscountCodes;
use eezeecommerce\DiscountBundle\Entity\Discounts;
use eezeecommerce\DiscountBundle\Form\DiscountCodesType;
use eezeecommerce\DiscountBundle\Form\DiscountsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use eezeecommerce\CategoryBundle\Entity\Category;
use eezeecommerce\CategoryBundle\Form\CategoryType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

class WebstoreController extends Controller
{
    /*
     * 
     */

    public function categoryAction()
    {
        $categories = $this->getDoctrine()
            ->getRepository('eezeecommerceCategoryBundle:Category')
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Webstore:category.html.twig", ["categories" => $categories]);
    }

    public function categoryAddAction(Request $request)
    {
        $category = new Category();
        $categorytype = new CategoryType();

        $form = $this->createForm($categorytype, $category)->add("Save", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $kernel = $this->get("kernel")->getRootDir();
            exec("php " . $kernel . "/console cache:clear --env=prod");

            return $this->redirectToRoute("_eezeecommerce_admin_webstore_category", ["id" => $category->getId()]);
        }

        return $this->render("eezeecommerceAdminBundle:Webstore:categoryadd.html.twig", ["form" => $form->createView()]);
    }

    public function categoryDataAction(Request $request)
    {
        $parent = $request->query->get("parent");

        $array = [];

        if ($parent == "#") {

            $categories = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:Category")->findBy([], ["position" => "ASC"]);
            foreach ($categories as $category) {
                if (null !== $category->getParent()) {
                    continue;
                }
                $child = count($category->getChildren()) > 0 ? true : false;
                if ($child) {
                    $state = [
                        "opened" => true
                    ];
                } else {
                    $state = [
                        "opened" => false
                    ];
                }

                $array[] = [
                    "id" => $category->getId(),
                    "text" => $category->getTitle(),
                    "children" => $child,
                    "state" => $state,
                    "li_attr" => array(
                        "data-id" => $category->getId(),
                        "data-type" => "category"
                    )
                ];
            }
        } else {
            $parentRepo = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:Category")->findBy(["parent" => $parent], ["position" => "ASC"]);
            foreach ($parentRepo as $p) {
                $child = count($p->getChildren()) > 0 ? true : false;
                if ($child) {
                    $state = [
                        "opened" => true
                    ];
                } else {
                    $state = [
                        "opened" => false
                    ];
                }
                $children = array();

                if (count($p->getChildren()) > 0) {
                    foreach($p->getChildren() as $child) {
                        $children[] = [
                            "id" => $child->getId(),
                            "text" => $child->getTitle(),
                            "li_attr" => array(
                                "data-id" => $child->getId(),
                                "data-type" => "category"
                            )
                        ];
                    }
                }
                $array[] = [
                    "id" => $p->getId(),
                    "text" => $p->getTitle(),
                    "children" => $children,
                    "state" => $state,
                    "li_attr" => array(
                        "data-id" => $p->getId(),
                        "data-type" => "category"
                    )
                ];
            }
        }

        return new JsonResponse($array);
    }

    public function categoryMoveAction(Request $request)
    {

        if (!$request->request->has("json")) {
            return new JsonResponse(false);
        }

        $json = json_decode($request->request->get("json"), true);

        $em = $this->getDoctrine()->getManager();

        $i = 0;
        foreach ($json as $categoryData) {
            $category = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:Category")
                ->find($categoryData["id"]);
            $category->setParent(null);
            $category->setPosition($i);
            $em->persist($category);
            $i2 = 0;
            foreach ($categoryData["children"] as $children) {
                $child = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:Category")
                    ->find($children["id"]);
                $child->setParent($category);
                $child->setPosition($i2);
                $em->persist($child);

                $i3 = 0;
                foreach ($children["children"] as $subchildren) {
                    $subChild = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:Category")
                        ->find($subchildren["id"]);
                    $subChild->setParent($child);
                    $subChild->setPosition($i3);
                    $em->persist($child);
                    $i3++;
                }

                $i2++;
            }

            $i++;
        }

        $em->flush();

        return new JsonResponse(true);
    }

    public function categoryDeleteAction(Request $request)
    {
        $id = $request->query->get("id");

        if (null === $id) {
            return;
        }

        $category = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:Category")
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        $kernel = $this->get("kernel")->getRootDir();
        exec("php " . $kernel . "/console cache:clear --env=prod");


        return new JsonResponse(true);
    }

    public function categoryModifyAction(Request $request, $id)
    {
        $categorytype = new CategoryType();
        $categories = $this->getDoctrine()
            ->getRepository('eezeecommerceCategoryBundle:Category')
            ->find($id);

        if (!$categories) {
            throw $this->createNotFoundException("Category Not Found");
        }

        $form = $this->createForm($categorytype, $categories)->add("Update", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $categories->setSlug($form["slug"]->getData());

            $em = $this->getDoctrine()->getManager();

            $em->persist($categories);
            $em->flush();

            $kernel = $this->get("kernel")->getRootDir();
            exec("php " . $kernel . "/console cache:clear --env=prod");

            return $this->redirectToRoute("_eezeecommerce_admin_webstore_category");
        }

        return $this->render("eezeecommerceAdminBundle:Webstore:categoryadd.html.twig", ["categories" => $categories, "form" => $form->createView()]);
    }

    public function discountsAction()
    {
        $discounts = $this->getDoctrine()->getRepository("eezeecommerceDiscountBundle:Discounts")
            ->findAll();

        $discountCodes = $this->getDoctrine()->getRepository("eezeecommerceDiscountBundle:DiscountCodes")
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Webstore:discounts.html.twig", ["discounts" => $discounts, "discountCodes" => $discountCodes]);
    }

    public function discountsAddAction(Request $request)
    {
        $discount = new Discounts();

        $form = $this->createForm(new DiscountsType(), $discount);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($discount);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_webstore_discounts");
        }

        return $this->render("eezeecommerceAdminBundle:Webstore:discountsadd.html.twig", ["form" => $form->createView()]);
    }

    public function discountsModifyAction($id, Request $request)
    {
        $discount = $this->getDoctrine()->getRepository("eezeecommerceDiscountBundle:Discounts")
            ->findOneById($id);

        $form = $this->createForm(new DiscountsType(), $discount);

        $originalDiscountAmounts = new ArrayCollection();

        foreach ($discount->getDiscountAmount() as $discountAmount) {
            $originalDiscountAmounts->add($discountAmount);
        }

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($originalDiscountAmounts as $amount) {
                if (false === $discount->getDiscountAmount()->contains($amount)) {
                    $em->remove($amount);
                }
            }

            $em->persist($discount);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_webstore_discounts");
        }

        return $this->render("eezeecommerceAdminBundle:Webstore:discountsadd.html.twig", ["form" => $form->createView()]);
    }

    public function discountsDeleteAction($id)
    {
        $discount = $this->getDoctrine()->getRepository("eezeecommerceDiscountBundle:Discounts")
            ->findOneById($id);

        $em = $this->getDoctrine()->getManager();

        $em->remove($discount);
        $em->flush();

        return $this->redirectToRoute("_eezeecommerce_admin_webstore_discounts");
    }

    public function discountCodeAddAction(Request $request)
    {
        $discount = new DiscountCodes();

        $form = $this->createForm(new DiscountCodesType(), $discount);

        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($discount);

        if (count($errors) > 0) {
            return $this->render("eezeecommerceAdminBundle:Webstore:discountcodeadd.html.twig", [
                "form" => $form->createView(),
                "errors" => $errors]);
        }

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($discount);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_webstore_discounts");
        }

        return $this->render("eezeecommerceAdminBundle:Webstore:discountcodeadd.html.twig", ["form" => $form->createView()]);
    }

    public function discountCodeModifyAction($id, Request $request)
    {
        $discount = $this->getDoctrine()->getRepository("eezeecommerceDiscountBundle:DiscountCodes")
            ->findOneById($id);

        $form = $this->createForm(new DiscountCodesType(), $discount);

        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($discount);

        if (count($errors) > 0) {
            return $this->render("eezeecommerceAdminBundle:Webstore:discountcodeadd.html.twig", [
                "form" => $form->createView(),
                "errors" => $errors]);
        }

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($discount);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_webstore_discounts");
        }

        return $this->render("eezeecommerceAdminBundle:Webstore:discountcodeadd.html.twig", ["form" => $form->createView()]);
    }

    public function discountCodeDeleteAction($id)
    {
        $discount = $this->getDoctrine()->getRepository("eezeecommerceDiscountBundle:DiscountCodes")
            ->findOneById($id);

        $em = $this->getDoctrine()->getManager();

        $em->remove($discount);
        $em->flush();

        return $this->redirectToRoute("_eezeecommerce_admin_webstore_discounts");
    }

    public function categoryJstreeAction()
    {
        $file = $this->renderView("eezeecommerceAdminBundle:Templates:jstree.js.twig");

        $response = new Response($file);
        $response->headers->set("Content-Type", "application/javascript");
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }

    public function categoryUitreeAction()
    {
        $file = $this->renderView("eezeecommerceAdminBundle:Templates:ui-tree.js.twig");

        $response = new Response($file);
        $response->headers->set("Content-Type", "application/javascript");
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }

    public function categoryPriceFilterAction()
    {
        $filters = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:CategoryPriceFilters")
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Webstore:price-filter-index.html.twig", array(
            "filters" => $filters
        ));
    }

    public function addCategoryPriceFilterAction(Request $request)
    {
        $filter = new CategoryPriceFilters();
        $form = $this->createForm(new CategoryPriceFiltersType(), $filter);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($filter);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_webstore_category_price");
        }

        return $this->render("eezeecommerceAdminBundle:Webstore:price-filter-add.html.twig", array(
            "form" => $form->createView()
        ));
    }

    public function editCategoryPriceFilterAction(Request $request, $id)
    {
        $filter = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:CategoryPriceFilters")
            ->find($id);
        $form = $this->createForm(new CategoryPriceFiltersType(), $filter);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($filter);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_webstore_category_price");
        }

        return $this->render("eezeecommerceAdminBundle:Webstore:price-filter-add.html.twig", array(
            "form" => $form->createView()
        ));
    }

}
