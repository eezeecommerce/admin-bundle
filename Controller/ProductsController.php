<?php

namespace eezeecommerce\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use eezeecommerce\ProductBundle\Entity\Product;
use eezeecommerce\ProductBundle\Entity\Options;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use eezeecommerce\ProductBundle\Form\ProductType;
use eezeecommerce\ProductBundle\Form\OptionsType;

class ProductsController extends Controller
{

    /**
     * @todo var $abc should be array orders injected (IGNORE)
     */
    public function indexAction()
    {
        return $this->render("eezeecommerceAdminBundle:Products:index.html.twig");
    }

    public function disabledAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('eezeecommerceProductBundle:Product')
            ->findDisabled();

        return $this->render("eezeecommerceAdminBundle:Products:disabled.html.twig", ["products" => $products]);
    }

    public function addProductAction(Request $request)
    {
        $product = new Product();
        $producttype = $this->get("eezeecommerce.form.product");

        $form = $this->createForm($producttype, $product)->add("Save", "submit");

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $product->setUri($form["uri"]->getData());

            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_products_details", ["prodnum" => $product->getId()]);
        }

        return $this->render(
            "eezeecommerceAdminBundle:Products:productsadd.html.twig",
            ["form" => $form->createView()]
        );
    }

    public function productDisableAction($id)
    {
        $product = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Product")
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $product->setDisabled(1);
        $em->persist($product);
        $em->flush();

        $this->addFlash(
            'success',
            "Item disabled successfully."
        );

        return $this->redirectToRoute("_eezeecommerce_admin_products_index");
    }

    public function productEnableAction($id)
    {
        $product = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Product")
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $product->setDisabled(0);
        $em->persist($product);
        $em->flush();

        $this->addFlash(
            'success',
            "Item enabled successfully."
        );

        return $this->redirectToRoute("_eezeecommerce_admin_products_index");
    }

    public function productDetailsAction(Request $request, $prodnum)
    {
        $producttype = $this->get("eezeecommerce.form.product");
        $products = $this->getDoctrine()
            ->getRepository('eezeecommerceProductBundle:Product')
            ->find($prodnum);

        if (!$products) {
            throw $this->createNotFoundException("Product Not Found");
        }

        $form = $this->createForm($producttype, $products)->add("Update", "submit");

        $form["stock"]->remove("stock");
        $form["stock"]->add("currentStock", null, array("label" => false));

        $originalVariants = new ArrayCollection();

        foreach ($products->getVariants() as $variant) {
            $originalVariants->add($variant);
        }

        $originalImages = new ArrayCollection();

        foreach ($products->getImage() as $image) {
            $originalImages->add($image);
        }

        $originalAttributes = new ArrayCollection();

        foreach ($products->getAttribute() as $attribute) {
            $originalAttributes->add($attribute);
        }

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($originalVariants as $variant) {
                if (false === $products->getVariants()->contains($variant)) {
                    $em->remove($variant);
                }
            }
            foreach ($originalImages as $image) {
                if (false === $products->getImage()->contains($image)) {
                    $em->remove($image);
                }
            }
            foreach ($originalAttributes as $attribute) {
                if (false === $products->getAttribute()->contains($attribute)) {
                    $em->remove($attribute);
                }
            }

            $products->setUri($form["uri"]->getData());

            $em->persist($products);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_products_index");
        }

        return $this->render(
            "eezeecommerceAdminBundle:Products:productdetails.html.twig",
            ["product" => $products, "form" => $form->createView()]
        );
    }

    public function inventoryAction()
    {
        $stock = $this->getDoctrine()->getRepository('eezeecommerceStockBundle:Stock')
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Products:inventory.html.twig", ["stock" => $stock]);
    }

    public function OptionsAction()
    {
        $options = $this->getDoctrine()
            ->getRepository('eezeecommerceProductBundle:Options')
            ->findAll();

        return $this->render("eezeecommerceAdminBundle:Products:Options/index.html.twig", ["options" => $options]);
    }

    public function OptionsAddAction(Request $request)
    {
        $option = new Options();
        $optiontype = $this->get("eezeecommerce.form.options");

        $form = $this->createForm($optiontype, $option)->add("Save", "submit");

        if ($form->handleRequest($request)->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($option);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_products_options_index");
        }

        return $this->render(
            "eezeecommerceAdminBundle:Products:Options/optionadd.html.twig",
            ["form" => $form->createView()]
        );
    }

    public function OptionsModifyAction(Request $request, $id)
    {
        $optiontype = $this->get("eezeecommerce.form.options");
        $option = $this->getDoctrine()
            ->getRepository('eezeecommerceProductBundle:Options')
            ->find($id);

        if (!$option) {
            throw $this->createNotFoundException("Product Option Not Found");
        }
        $form = $this->createForm($optiontype, $option)->add("Update", "submit");

        $originalChoices = new ArrayCollection();

        foreach ($option->getOptionChoices() as $choice) {
            $originalChoices->add($choice);
        }

        if ($form->handleRequest($request)->isValid()) {

            $em = $this->getDoctrine()->getManager();
            foreach ($originalChoices as $choice) {
                if (false === $option->getOptionChoices()->contains($choice)) {
                    $em->remove($choice);
                }
            }


            $em->persist($option);
            $em->flush();

            return $this->redirectToRoute("_eezeecommerce_admin_products_options_index");
        }

        return $this->render(
            "eezeecommerceAdminBundle:Products:Options/optiondetail.html.twig",
            ["option" => $option, "form" => $form->createView()]
        );
    }

    public function productArrayAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('eezeecommerceProductBundle:Product')
            ->findProducts();
        $array = array();
        foreach ($products as $key => $product) {
            $tmp = array();
            $id = null;
            foreach ($product as $key => $value) {
                if ($key == "id") {
                    $id = $value;
                }
                if ($key == 'product_name') {
                    $tmp[] = "<a href='".$this->generateUrl(
                            "_eezeecommerce_admin_products_details",
                            array("prodnum" => $id)
                        )."'>".$value."</a>";
                } else {
                    $tmp[] = $value;
                }
            }
            $array["data"][] = $tmp;
        }


        return new JsonResponse($array);
    }

    public function productDisabledArrayAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('eezeecommerceProductBundle:Product')
            ->findDisabledArray();
        $array = array();
        foreach ($products as $key => $product) {
            $tmp = array();
            $id = null;
            foreach ($product as $key => $value) {
                if ($key == "id") {
                    $id = $value;
                }
                if ($key == 'product_name') {
                    $tmp[] = "<a href='".$this->generateUrl(
                            "_eezeecommerce_admin_products_details",
                            array("prodnum" => $id)
                        )."'>".$value."</a>";
                } else {
                    $tmp[] = $value;
                }
            }
            $array["data"][] = $tmp;
        }


        return new JsonResponse($array);
    }

}
