<?php

/* Copyright Astral Game Servers Ltd 2013-2014
 * Developed By
 * Liam Sorsby
 */

namespace eezeecommerce\AdminBundle\Services;

use eezeecommerce\DatabaseBundle\Model\SettingsQuery;

/**
 * Description of SettingsService
 *
 * @author root
 */
class SettingsService
{
    protected $settings;
    
    public function __construct(SettingsQuery $settings) {
        $this->settings = $settings::create()->findOne();
    }
    
    public function getVatInclusivePrices()
    {
        return $this->settings->getVatInclusivePrices();
    }
    
    public function getCustomerAccounts()
    {
        return $this->settings->getCustomerAccounts();
    }
    
    
}
