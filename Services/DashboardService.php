<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\AdminBundle\Services;

use eezeecommerce\DatabaseBundle\Model\OrderQuery;

/**
 * Description of DashboardService
 *
 * @author root
 */
class DashboardService
{

    private $orderQuery;
    
    protected $todayDate, $backDate, $currencyCode;

    public function setQuery(OrderQuery $orderQuery)
    {
        $this->orderQuery = $orderQuery;
    }
    
    public function setTodayDate()
    {
        $this->todayDate = date("Y-m-d");
    }

    /**
     * $range variable same as "last X days" if enter 1 it'll select today's range, 7 will return week, 14 two week's etc.
     * @param type $range
     */
    public function setRange($range)
    {
        if (!is_int($range)) {
            throw new \InvalidArgumentException("Range variable expected integer. Instead got: " . gettype($range));
        }
        
        $this->backDate = date("Y-m-d", time() - (60 * 60 * ($range * 24)));
        
    }
    
    public function getTodayDate()
    {
        return $this->todayDate;
    }
    
    public function getOrdersCount()
    {

        return $this->orderQuery
                ->filterByOrderdate(array("min" => $this->todayDate . "00:00:00", "max" => $this->backDate . "23:59:59"))
                ->count();
    }
    
    /**
     * Accepts $code e.g. "GBP" / "USD"
     * @param type $code
     */
    public function setCurrency($code)
    {
        if (!is_string($code)) {
            if (strlen($code) !== 3) {
               throw new \InvalidArgumentException("Code variable expected 3 chars long. Instead got: " . strlen($code)); 
            }
            throw new \InvalidArgumentException("Code variable expected string. Instead got: " . gettype($code));
        }
        $this->currencyCode = $code;
    }
    
    public function setConversionRate()
    {
        return $this->orderQuery;
    }
    
    public function getSalesTotals()
    {
        // handle currencies
        // add all values together after converting
        // run sql
        // get sum of all values and return
    }
    
    

}
