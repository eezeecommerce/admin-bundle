<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\AdminBundle\Twig;

use Twig_Extension;
use Twig_SimpleFilter;
/**
 * Description of UnserializeExtension
 *
 * @author root
 */
class UnserializeExtension extends Twig_Extension
{
    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('unserialize', array($this, "unserializeFilter")),
        );
    }
    
    public function unserializeFilter($string)
    {
        return unserialize($string);
    }
    
    public function getName()
    {
        return "unserialize";
    }

}
