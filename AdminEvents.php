<?php

namespace eezeecommerce\AdminBundle;

class AdminEvents
{
    const ADMIN_MENU_INITIALISE = "eezeecommerce.admin.menu.initialise";

    const ADMIN_MENU_COMPLETED = "eezeecommerce.admin.menu.completed";
}