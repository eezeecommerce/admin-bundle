# **Configuration** #

Register the bundle in the AppKernel

```
#!php
new eezeecommerce\AdminBundle\eezeecommerceAdminBundle(),
```


add routing to app/config/routing.yml

```
#!yaml
eezeecommerce_admin:
    resource: "@eezeecommerceAdminBundle/Resources/config/routing.yml"
    prefix:   /admin
```