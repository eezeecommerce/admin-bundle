<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14/06/16
 * Time: 16:50
 */

namespace eezeecommerce\AdminBundle\Event;


use Symfony\Component\EventDispatcher\Event;

class AdminMenuEvent extends Event
{
    /**
     * Menu Array
     * @var array
     */
    private $menu;

    /**
     * AdminMenuEvent constructor.
     * @param array $menu Array of Menu Items
     */
    public function __construct(array $menu)
    {
        $this->menu = $menu;
    }

    /**
     * Returns Admin Menu
     *
     * @return array
     */
    public function getMenu()
    {
        return $this->menu;
    }

    public function setMenu(array $menu)
    {
        $this->menu = $menu;
    }
}